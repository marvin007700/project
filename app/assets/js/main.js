var device = device.default;

$(document).ready(function () {

if(!device.mobile() && !device.tablet()){
	// parallax
	(function(){
		var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});
		new ScrollMagic.Scene({triggerElement: '.more-info-bg'})
		.setTween('.more-info-bg__img', {y: '300px', ease: Linear.easeNone})
		.addTo(controller);
	})();

	if (Modernizr.mq('(min-width: 951px)')){
		// fixing svg
		(function(){
			var height = $('#info-profit__img').innerHeight() + 125,
				 offset = $(window).height() / 2;

			var controller = new ScrollMagic.Controller(),
			    scene = new ScrollMagic.Scene({triggerElement: '#more-info-dark__item-wrap', duration: height, offset: 250})
			.setPin('#info-profit__img')
			.addTo(controller);

			var waypoint = new Waypoint.Inview({
				element: $('.info-security'),
				entered: function(direction){
					if(direction == 'down'){
						$('#info-profit__img .info-profit__img-icon').addClass('showed');
					}
					if(direction == 'up'){
						$('#info-profit__img .info-profit__img-icon').removeClass('showed');
					}
				}
			});
		})();
	}

	//header fadeIn
	$('.header').addClass('fadeIn');

    //video-bg-block_init
	$(window).load(function(){
		$('.video-bg-block').addClass('video-bg-block_init');
	});

	//waypoints
	$('.animated-fadeInUp').each(function(index, element){
		$(element).waypoint(function(direction){
			if (direction == 'down'){
				$(element).addClass('animated fadeInUp');
			}
		}, {
			offset: '95%'
		});		
	});

	$('.about-scheme').waypoint(function(direction){
		if (direction == 'down'){
			$('.about-scheme__item').addClass('animated fadeInUp');
		}
	}, {
		offset: '95%'
	});	
}

//fixed header
(function () {
	var previousScroll = 0,
	    $headerFixed = $('#header-fixed'),
	    $header = $('#header');

	$(window).scroll(function(){
		var currentScroll = $(this).scrollTop();
		if (currentScroll < previousScroll){
			if (currentScroll > $header.offset().top + $header.outerHeight()){
				$headerFixed.addClass('header-fixed-active');
			} else{
				$headerFixed.removeClass('header-fixed-active');
			}
		} else{
			$headerFixed.removeClass('header-fixed-active');
		}
		previousScroll = currentScroll;
	});
}());

//menu
(function() {
	var $menu = $('#menu'),
		 $body = $('body'),
		 $html = $('html'),
		 $open = $('.menu-hamburger-button'),
		 $close = $('.menu__actions-item-close, #menu__shadow'),
		 $headerFixed = $('#header-fixed-inner'),
		 $btnBuy = $('.button-buy');

	$open.on('click', function(e){
		e.preventDefault();
		if (!$body.hasClass('menu_opened')){
			$body.removeClass('menu-form_opened');
			openMenu();
		} else{
			closeMenu();
		}
	});
	$close.on('click', function(e){
		e.preventDefault();
		closeMenu();
	});

	$('.menu__list-link').on('click', function(e){
		var top = 0,
		    offsetTop = $(this.hash).offset().top;

		if ($(window).scrollTop() > offsetTop){
			top = $('#header-fixed').outerHeight();
		} else{
			top = 0;
		}
		e.preventDefault();
		var hash = this.hash;
		$('html,body').stop().animate({
			scrollTop: offsetTop - top
		}, 700, 'easeInOutQuart', function(){
			closeMenu();
		});
	});

	//button-buy
	$btnBuy.on('click', function(e){
		e.preventDefault();
		if (!$body.hasClass('menu_opened')){
			$body.addClass('menu-form_opened');
			openMenu();
		}
	});

	//button-buy
	$('.menu__actions-item-buy, .menu__button-buy-mobile').on('click', function(e){
		e.preventDefault();
		$body.addClass('menu-form_opened');
	});

	function openMenu(){
		var oldWidth = $body.outerWidth(),
		    oldScrollTop = $html.scrollTop();;
		$menu.stop().fadeIn(function(){
			validate();
		});
		$body.addClass('menu_opened');
		var newWidth = $body.outerWidth();
		var res = newWidth - oldWidth;
		$body.css({
			'marginRight': res + 'px'
		});
		$html.scrollTop(oldScrollTop);
		$headerFixed.css('right', res + 'px');
	}
	function closeMenu(){
		$menu.stop().fadeOut();
		$body.removeClass('menu_opened').css({
			'marginRight': 0
		});
		$headerFixed.css('right', 0);
	}
})();

//video modal
$('.info-links__item_type_video').on('click', function(e){
	e.preventDefault();
	$('#video-modal').arcticmodal();
});

//team__slider
$('.team__slider').owlCarousel({
	smartSpeed: 1000,
	items: 3,
	margin: 36,
	loop: true,
	dots: false,
	nav: true,
	responsive:{
		1200:{
			items: 3,
			margin: 36,
			dots: false,
			nav: true
		},
		1024:{
			items:3,
			margin: 20,
			dots: false,
			nav: true
		},
		951:{
			items: 3,
			// autoWidth: true,
			margin: 20,
			dots: false,
			nav: true
		},
		320:{
			items: 1,
			// autoWidth: true,
			margin: 20,
			dots: true,
			nav: false
		}
	}
});

//timer
(function(){
	var $timer = $('.timer__block');

	var dateObj = new Date(),
		date = dateObj.getDate(),
		month = dateObj.getMonth(),
		year = dateObj.getFullYear(),
		hours = dateObj.getHours(),
		min = dateObj.getMinutes(),
		sec = dateObj.getSeconds(),
		milliSec = dateObj.getMilliseconds(),
		fullDate;

	if ($.cookie('date')){
		fullDate = $.cookie('date');
	} else{
		fullDate = dateObj;
	}

	var daysInMonth = moment(fullDate, "dddd, MMMM Do YYYY, h:mm:ss a").daysInMonth(),
	    countDate = date + 2,
	    countMonth = month;

	   if (countDate > daysInMonth){
	   	countMonth = countMonth + 1;
	   	countDate -= daysInMonth;
	   }

	var austDay = new Date();
	austDay = new Date(year, countMonth, countDate, hours, min, sec);

	var countDate;

	if ($.cookie('date')){
		countDate = $.cookie('date');
	} else{
		$.cookie('date', austDay, { expires: austDay });
		countDate = austDay;
	}

	$timer.countdown({
		until: new Date(countDate),
		onExpiry: function(){
		}
	});

})();

//validate
function validate(){
	$('#confirm-form').validate({
		rules: {
			confirm: {
				required: true
			},
			agree: {
				required: true
				}
		},
		submitHandler: function(form){
			$('#confirm-form').hide();
			$('#invest-modal').fadeIn();
			return false;
		}
	});
}

$('#subscribe__form-top').validate({
	submitHandler: function(form) {
		$(form).find('.subscribe__thanks').show();
		return false;
	}	
});
$('#subscribe__form-bottom').validate({
	submitHandler: function(form) {
		$(form).find('.subscribe__thanks').show();
		return false;
	}	
});
$('.subscribe__thanks-close').on('click', function(){
	$(this).parents('.subscribe__thanks').hide();
	$(this).parents('.subscribe__form').find('.subscribe__input').removeClass('valid').val('');
});

//confirm-form
(function(){
	var checkbox = $('#confirm-form input[type="checkbox"]'),
	    form = $('#confirm-form'),
	    btn = $('#confirm-form .confirm-form__button');

	checkbox.on('change', function(e){
		if (checkbox.filter(':checked').length == checkbox.length){
			btn.removeClass('button_disabled');
		} else{
			btn.addClass('button_disabled');
		}
	});
})();

});

$(window).load(function(){

$('.collapse-item').each(function(index, element){
	var hidden = $(element).find('.collapse-item__body');
	var height = hidden.height();
	hidden.css({
		'height': '0px',
		'position': 'static',
		'visibility': 'visible'
	});
	slideEl('.collapse-item__title:eq('+index+')', 'collapse-item__active', height, true);
});

function slideEl(clickEl, className, height, collapse){
	if (!$(clickEl).length) return;

	$(document).on('click', clickEl, function(e){
		e.preventDefault();
		var thisEl = $(this),
			 currentItem = thisEl.parent(),
			 parent = thisEl.parents('.collapse'),
			 collapseBody = parent.find('.collapse-item__body');

		if (!currentItem.hasClass(className)){
			currentItem.addClass(className);
			if (collapse == true){
				currentItem.siblings().find('.collapse-item__body').stop().animate({
					height: '0px'
				}, 500, 'easeInOutQuart'
				);
				currentItem.siblings().removeClass(className);
			}
			thisEl.siblings('.collapse-item__body').stop().animate({
				height: height+'px'
			}, 500, 'easeInOutQuart'
			);
		} else{
			currentItem.removeClass(className);
			thisEl.siblings('.collapse-item__body').stop().animate({
				height: '0px'
			}, 500, 'easeInOutQuart'
			);
		}

	});
}

dropDownEl('.select-lang__title', 'dropdown-menu_active');

function dropDownEl(clickEl, className){
	if (!$(clickEl).length) return;

	$(clickEl).on('click', function(e){
		e.preventDefault();
		e.stopPropagation();
		var thisEl = $(this),
			parent = $(this).parent();

		if ($('.' + className).length && !parent.hasClass(className)){
			$('.' + className).removeClass(className).find('.dropdown-menu').stop().fadeOut();
		}
		if (!parent.hasClass(className)){
			parent.addClass(className);
			thisEl.siblings('.dropdown-menu').stop().fadeIn();
		} else{
			parent.removeClass(className);
			thisEl.siblings('.dropdown-menu').stop().fadeOut();
		}
	});
	$(document).on('click', function(event){
		var target = $(event.target);

		if ($('.' + className).length){
			if (target.closest('.' + className).length) return;

			$('.' + className).removeClass(className);
			$(clickEl).siblings('.dropdown-menu').stop().fadeOut();
		}
	});
}

$(document).on('click', '.select-lang__list-item_active, .dropdown-menu__title', function(e){
    e.preventDefault();
	var thisEl = $(this),
		parent = thisEl.parents('.dropdown-menu_active');

	parent.removeClass('dropdown-menu_active');
	parent.find('.dropdown-menu').stop().fadeOut();
});

});