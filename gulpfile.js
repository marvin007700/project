'use strict';

var	gulp = require('gulp'),
		browserSync = require('browser-sync').create(),
		plumber = require('gulp-plumber'),
		notify = require('gulp-notify'),
		compass = require('gulp-compass'),
		useref = require('gulp-useref'),
		gulpif = require('gulp-if'),
		pug = require('gulp-pug'),
		htmlbeautify = require('gulp-html-beautify'),
		autoprefixer = require('gulp-autoprefixer'),
		spritesmith = require('gulp.spritesmith'),
		imagemin = require('gulp-imagemin'),
		pngquant = require('imagemin-pngquant'),
		tinypng = require('gulp-tinypng'),
		svgSprite = require('gulp-svg-sprite'),
		svgmin = require('gulp-svgmin'),
		cheerio = require('gulp-cheerio'),
		replace = require('gulp-replace'),
		cssmin = require('gulp-cssmin'),
		uglify = require('gulp-uglify'),
		concat = require('gulp-concat'),
		sourcemaps = require('gulp-sourcemaps'),
		rename = require('gulp-rename'),
		runSequence = require('run-sequence'),
		clean = require('gulp-clean');

//path
var app_path = 'app',
	 dist_path = 'dist',
	 app_css_path = app_path+'/assets/css',
    app_js_path = app_path+'/assets/js',
    app_img_path = app_path+'/assets/img',
    dist_css_path = dist_path+'/assets/css',
    dist_js_path = dist_path+'/assets/js',
    dist_img_path = dist_path+'/assets/img',
    pug_path = app_path+'/pug',
    sass_path = app_path+'/sass',
    sprite_path = app_img_path+'/sprite',
    sprite_sass_path = sass_path+'/sprite',
    sprite_template = sass_path+'/sprite/templates',
    base64_path = app_img_path+'/base64',
    bower_path = app_path+'/bower_components';

//static server + watching files
gulp.task('serve', function() {

	browserSync.init({
		server: app_path
	});

	gulp.watch(sass_path+"/**/*.scss", ['compass']);
	//gulp.watch(app_css_path+"/main.css", ['autoprefixer']);
	gulp.watch(pug_path+"/**/*.pug", ['pug']);
	gulp.watch(app_js_path+"/**/*.js").on('change', browserSync.reload);
	gulp.watch(app_path+"/*.html").on('change', browserSync.reload);
});

//compass
gulp.task('compass', function() {
	gulp.src(sass_path+'/**/*.scss')
		.pipe(plumber({errorHandler: notify.onError()}))
		.pipe(compass({
		config_file: 'config.rb',
		css: app_css_path,
		sass: sass_path
		}))
		.pipe(gulp.dest(app_css_path))
		.pipe(browserSync.stream());
});

//pug
gulp.task('pug', function() {
	gulp.src(pug_path+'/pages/*.pug')
		.pipe(plumber({errorHandler: notify.onError()}))
		.pipe(pug({pretty: true}))
		.pipe(gulp.dest(app_path))
		.pipe(browserSync.stream());
});

const BROWSERS = [
	'ie >= 10',
	'ie_mob >= 10',
	'ff >= 30',
	'chrome >= 34',
	'safari >= 7',
	'opera >= 23',
	'ios >= 7',
	'android >= 4.4',
	'bb >= 10'
];
// const BROWSERS = ['last 2 versions'];

//autoprefixer
gulp.task('autoprefixer', function () {
	gulp.src(dist_css_path+'/main.css')
		// .pipe(plumber({errorHandler: notify.onError()}))
		.pipe(autoprefixer({
			browsers: BROWSERS,
			remove: false,
			cascade: false
		}))
		.pipe(gulp.dest(dist_css_path))
		/*.pipe(browserSync.stream())*/;
});

//sprite
var dateObj = new Date();

var date = dateObj.getDate(),
	 month = dateObj.getMonth()+1,
    year = dateObj.getFullYear(),
    hours = dateObj.getHours(),
    min = dateObj.getMinutes(),
    sec = dateObj.getSeconds();

var spriteName = date+'-'+month+'-'+year+'-'+hours+'-'+min+'-'+sec;

gulp.task('sprite', function () {
	var spriteData = gulp.src([sprite_path+'/*.*', '!'+sprite_path+'/*.svg'])
		.pipe(spritesmith({
			imgName: 'sprite-'+spriteName+'.png',
			cssName: '_sprite-'+spriteName+'.scss',
			padding: 0,
			algorithm: 'left-right',//top-down left-right diagonal alt-diagonal binary-tree
			algorithmOpts: {sort: false},
			cssTemplate: sprite_template+'/sprite.scss.handlebars'
		}));
		spriteData.img
		.pipe(gulp.dest(app_img_path));
		spriteData.css.pipe(gulp.dest(sprite_sass_path));
});

//sprite-svg
gulp.task('sprite-svg', function() {
	gulp.src(sprite_path+'/*.svg')
   .pipe(svgmin({
     js2svg: {
       pretty: true
     }
   }))
   // .pipe(cheerio({
   //   run: function ($) {
   //     $('[fill]:not([fill="none"])').removeAttr('fill');
   //     $('[stroke]').removeAttr('stroke');
   //     $('[style]').removeAttr('style');
   //   },
   //   parserOptions: { xmlMode: true }
   // }))
   // .pipe(replace('&gt;', '>'))
   .pipe(svgSprite({
		mode: {
			symbol: {
				// sprite: "../sprite-svg.svg",
				sprite: '../sprite-'+spriteName+'.svg',
				render: {
					scss: {
						// dest: '../../sass/sprite/_sprite-svg.scss',
						dest: '../../../sass/sprite/_sprite-svg-'+spriteName+'.scss',
						template: sprite_template+'/_sprite-svg-template-new.scss'
					}
				}
			}
		}	
   }))
   .pipe(gulp.dest(app_img_path))
});

//optimize svg
gulp.task('img-min-svg', function () {
	gulp.src([dist_img_path+'/**/*.svg'])
	.pipe(imagemin({
      progressive: true,
      interlaced: true,
		svgoPlugins: [{removeViewBox: false}],
		use: [pngquant()]
    }))
    .pipe(gulp.dest(dist_img_path));
});

//optimize images
gulp.task('img-min', function () {
    gulp.src([dist_img_path+'/**/*.{png,jpg,jpeg}'])
        .pipe(tinypng('eWaFQ9kLQPOhh3vOSoSfZAvfJwHfL-QI'))
        .pipe(gulp.dest(dist_img_path));
});

//htmlbeautify
gulp.task('htmlbeautify', function() {
	var options = {
		indent_size: 2,
		indent_char: "\t",
		// eol: "\n",
		// indent_level: 0,
		indent_with_tabs: true,
		// preserve_newlines: true,
		// max_preserve_newlines: 10,//1
		// jslint_happy: false,
		// space_after_anon_function: false,
		// brace_style: "collapse",//expand
		// keep_array_indentation: false,
		// keep_function_indentation: false,
		// space_before_conditional: true,
		// break_chained_methods: false,
		// eval_code: false,
		// unescape_strings: false,
		// wrap_line_length: 0,
		// wrap_attributes: "auto",
		// wrap_attributes_indent_size: 4,
		// end_with_newline: false,//true
		unformatted: [
			// https://www.w3.org/TR/html5/dom.html#phrasing-content
			'abbr', 'area', 'b', 'bdi', 'bdo', 'br', 'cite',
			'code', 'data', 'datalist', 'del', 'dfn', 'em', 'embed', 'i', 'ins', 'kbd', 'keygen', 'map', 'mark', 'math', 'meter', 'noscript',
			'object', 'output', 'progress', 'q', 'ruby', 's', 'samp', 'small',
			'strong', 'sub', 'sup', 'template', 'time', 'u', 'var', 'wbr', 'text',
			'acronym', 'address', 'big', 'dt', 'ins', 'strike', 'tt'
		]
	};
	gulp.src(dist_path+'/*.html')
	.pipe(htmlbeautify(options))
	.pipe(gulp.dest(dist_path))
});

//clean-dist
gulp.task('clean-dist', function () {
	return gulp.src(dist_path, {read: false})
	.pipe(clean());
});

//move
var move = [
	app_path+'/**',
	'!'+app_path+'/*.html',
	'!'+bower_path+'/**','!'+bower_path,
	'!'+pug_path+'/**', '!'+pug_path,
	'!'+sass_path+'/**', '!'+sass_path,
	'!'+app_css_path+'/**', '!'+app_css_path,
	'!'+app_js_path+'/**', '!'+app_js_path,
	'!'+sprite_path+'/**', '!'+sprite_path,
	'!'+base64_path+'/**', '!'+base64_path,
];

//move dev dir
gulp.task('move-dev', function () {
	return gulp.src(move)
	.pipe(gulp.dest(dist_path))
});

function moveFiles(src, name, distPath){
	gulp.task('move-files', function () {
		return gulp.src(src)
		.pipe(rename(name))
		.pipe(gulp.dest(distPath));
	});
	gulp.start('move-files');
}

////////////////////////////////////////////////////////
// BUILD
////////////////////////////////////////////////////////

//build useref({noconcat:true})
gulp.task('build', function () {
	return gulp.src(app_path+'/*.html')
	.pipe(useref())
	.pipe(gulpif('*.js', uglify()))
	.pipe(gulpif('*.css', cssmin()))
	.pipe(gulp.dest(dist_path));
});

gulp.task('build-custom', function () {
	return gulp.src(dist_path+'/*.html')
	.pipe(useref({
		custom: function (content, target, options, alternateSearchPath) {
			var str = content.replace(/\s/g, ''),
				 strLink = str.substr(1, 4),
				 strScript = str.substr(1, 6),
				 pathArr = target.split('/');

				 var pathRemove = pathArr.slice(0, -1);
				 var pathStr = pathRemove.join('/');
				 var name = pathArr.pop();

			if (strLink == 'link') {
				content = content.replace(/href="([^"]*)"/i, 'href="'+target+'"');
			} else if(strScript == 'script'){
				content = content.replace(/src="([^"]*)"/i, 'src="'+target+'"');
			} else{
				content = content;
			}
			moveFiles(app_path+'/'+alternateSearchPath, name, dist_path+'/'+pathStr);
			return content;
  		}
	}))
	.pipe(gulp.dest(dist_path));
});

gulp.task('default', ['clean-dist'], function (cb) {
  runSequence('move-dev', 'build', 'build-custom', 'htmlbeautify', 'autoprefixer', 'img-min', 'img-min-svg', cb);
});